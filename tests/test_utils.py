#  Copyright (c) 2021. Michael Kemna.

import pytest

from powderbooking.utils import get_env_or_raise


@pytest.mark.parametrize(
    "env_name,env_value",
    [
        ("PROJECT_NAME", "powderbooking"),
        ("POSTGRESQL_USER", "user"),
    ],
)
def test_get_env_or_raise_success(monkeypatch, env_name: str, env_value: str):
    # arrange
    monkeypatch.setenv(env_name, env_value)

    # act
    value = get_env_or_raise(env_name)

    # assert
    assert value is not None
    assert value == env_value


@pytest.mark.parametrize(
    "env_name",
    [
        "PROJECT_NAME",
        "POSTGRESQL_USER",
    ],
)
def test_get_env_or_raise_fail_none(monkeypatch, env_name: str):
    # arrange
    monkeypatch.delenv(env_name, raising=False)

    # act
    # assert
    with pytest.raises(EnvironmentError):
        get_env_or_raise(env_name)


@pytest.mark.parametrize(
    "env_name",
    [
        "PROJECT_NAME",
        "POSTGRESQL_USER",
    ],
)
def test_get_env_or_raise_fail_empty(monkeypatch, env_name: str):
    # arrange
    monkeypatch.setenv(env_name, "")

    # act
    # assert
    with pytest.raises(EnvironmentError):
        get_env_or_raise(env_name)
