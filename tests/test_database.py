#  Copyright (c) 2021. Michael Kemna.

import pytest
from mock import Mock
from sqlalchemy import Table, Column

from powderbooking.database import model_database, DatabaseHandler, build_database_url

TABLE_NAMES = ["resort", "weather", "forecast", "forecast_week"]

ENVIRONMENTAL_VARIABLES = [
    "PROJECT_NAME",
    "POSTGRESQL_USER",
    "POSTGRESQL_PASSWORD",
    "POWDERBOOKING_POSTGRESQL_SERVICE_HOST",
    "POWDERBOOKING_POSTGRESQL_SERVICE_PORT",
    "POSTGRESQL_DATABASE",
]


@pytest.fixture(scope="module")
def db():
    mocked_engine = Mock()
    return DatabaseHandler(*model_database(Mock())), mocked_engine


@pytest.fixture(scope="function")
def database_env(monkeypatch):
    monkeypatch.setenv("PROJECT_NAME", "powderbooking")
    monkeypatch.setenv("POSTGRESQL_USER", "user")
    monkeypatch.setenv("POSTGRESQL_PASSWORD", "password")
    monkeypatch.setenv("POWDERBOOKING_POSTGRESQL_SERVICE_HOST", "host")
    monkeypatch.setenv("POWDERBOOKING_POSTGRESQL_SERVICE_PORT", "5432")
    monkeypatch.setenv("POSTGRESQL_DATABASE", "database")


@pytest.mark.parametrize("table_name", TABLE_NAMES)
def test_get_table_success(db, table_name):
    """
    Verify that the tables are registered on the metadata.
    """
    db_handler, _ = db
    result = db_handler.get_table(table_name)

    assert isinstance(result, Table)


def test_get_table_fail(db):
    """
    Verify that it will indeed raise a ValueError if not found.
    """
    db_handler, _ = db
    with pytest.raises(ValueError):
        db_handler.get_table("not_in_db")


@pytest.mark.parametrize("table_name", TABLE_NAMES)
def test_get_table_column_success(db, table_name):
    """
    Verify that the function retrieves generic id column.
    This is not exhaustive, just to verify behavior of the function.
    """
    db_handler, _ = db
    result = db_handler.get_table_column(table_name, "id")

    assert isinstance(result, Column)


@pytest.mark.parametrize("table_name", TABLE_NAMES)
def test_get_table_column_fail(db, table_name):
    """
    Verify that it will indeed raise a ValueError if not found.
    """
    db_handler, _ = db
    with pytest.raises(ValueError):
        db_handler.get_table_column(table_name, "not_in_table")


def test_build_database_url_success(database_env):
    # arrange
    # act
    result = build_database_url()

    # assert
    assert result == f"postgresql://user:password@host:5432/database"


@pytest.mark.parametrize("env_name", ENVIRONMENTAL_VARIABLES)
def test_build_database_url_fail(database_env, monkeypatch, env_name):
    # arrange
    monkeypatch.delenv(env_name)

    # act
    # assert
    with pytest.raises(EnvironmentError):
        build_database_url()
