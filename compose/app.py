#  Copyright (c) 2021. Michael Kemna.
from database import build_database_handler, build_database_url

if __name__ == "__main__":
    url = build_database_url()
    db = build_database_handler(url)
    print("successfully initiated the database.")
    print(f"the database is accessible at: {url}")
